#include<iostream>

using namespace std;

//shows table
void table(int grid[6][7]){
	for(int i=1; i<=7; i++){
		cout<<" "<<i<<" ";
	}cout<<endl;
	for(int i=0;i<6;i++){
		for(int j=0;j<7;j++){
			cout<<"[";
			if(grid[i][j]==0){
				cout<<" ";
			}else if(grid[i][j]==1){
				cout<<"X";
			}else{
				cout<<"O";
			}
			cout<<"]";
		}
	    cout<<endl;	
	}
}

//input value
void pushValue(int grid[6][7] ,int player){
	int input;//user input column number
	//user input column
 	cout<<"Player "<<player<<" choose column between 1 to 7: ";
 	cin>>input;
 	//push value to array
 	for(int row=5; row>=0; row--){
 		if (grid[row][input-1]==0){
 			grid[row][input-1]=player;
 			break;
 		}
	 }
}
//check table
int checkTable(int grid[6][7],int player){
	//check full table
	bool loop=true;
	bool full=true;
	for(int i=0; i<6; i++){//row
		for(int j=0; j<7; j++){//column
			if(grid[i][j]==0){ //table is not full
				full=false;
				loop=false;
				break;
			}
		}
		if(loop==false){
			break;
		}
	}
	//---------------------------
	if(full==true){
		return 3;
	}
	//check vertical
	for(int i=5; i>=3; i--){//row
		for(int j=0; j<=6; j++){//column
			if(grid[i][j]==grid[i-1][j] && grid[i][j]!=0){
				if(grid[i-1][j]==grid[i-2][j]){
					if(grid[i-2][j]==grid[i-3][j]){
						return player;
					}
				}
			}
		}
	}
	//check horizontal
	for(int i=0; i<=5; i++){//row
		for(int j=0; j<=3; j++){//column
			if(grid[i][j]==grid[i][j+1]&& grid[i][j]!=0){
				if(grid[i][j+1]==grid[i][j+2]){
					if(grid[i][j+2]==grid[i][j+3]){
						return player;
					}
				}
			}
		}
	}
	//check diagonally left to right
	 for(int i=0;i<=2;i++){
	 	for(int j=0;j<=3;j++){
	 		if(grid[i][j]==grid[i+1][j+1]&&grid[i][j]!=0){
	 			if(grid[i+1][j+1]==grid[i+2][j+2]){
	 				if(grid[i+2][j+2]==grid[i+3][j+3]){
	 					return player;
					 }
				 }
			 }
		 }
	 }
	//check diagonally right to left
	for(int i=5;i>=3;i--){
		for(int j=0;j<=3;j++){
			if(grid[i][j]==grid[i-1][j+1]&&grid[i][j]!=0){
				if(grid[i-1][j+1]==grid[i-2][j+2]){
					if(grid[i-2][j+2]==grid[i-3][j+3]){
						return player;
					}
				}
			}
		}
	}
	
	//still no one win
	return 0;
}
//switch player
void switches(int&player){
	if(player==1){
		player=2;
	}else{
		player=1;
	}  
}
int main(){
cout<<"Connect 4"<<endl<<endl;
cout<<"Player 1 = X\nPlayer 2 = O"<<endl<<endl;


//declare variable
 int grid[6][7]={};
 int win=0; //check game status; 0=game still play,1=player1 wins,2=player2 wins,3=draw
 int player=1;//show turn of a player
 
 while(win==0){
 	table(grid); //shows table
 	pushValue(grid,player); //push value to array
    win=checkTable(grid,player);//check table if its full or win  
	switches(player); //switch user 
 }
 table(grid); //shows table
 //show game status 
 if(win==1){
 	cout<<"Player1 Wins!";
 }else if(win==2){
 	cout<<"Player2 Wins!";
 }else if(win==3){
 	cout<<"Draw";
 }
 

 
return 0;
 
}
